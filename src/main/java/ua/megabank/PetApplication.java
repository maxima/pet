package ua.megabank;

@SpringBootApplication
@EnableAspectJAutoProxy
@Slf4j
public class PetApplication {

    public static void main(String[] args) {
        try {
            SpringApplication.run(PetApplication.class, args);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
